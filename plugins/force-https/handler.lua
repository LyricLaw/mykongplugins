local ForceHttps = {
  VERSION = "1.0.0",
  PRIORITY = 1899,
}

function ForceHttps:access(conf)
    if kong.request.get_forwarded_scheme() ~= "https" then
        local redirect_url = "https://" .. kong.request.get_forwarded_host() .. kong.request.get_path_with_query()
        return kong.response.exit(301, "Redirecting to ".. redirect_url .. "...", {
            ["Location"] = redirect_url
        })
    end
end

return ForceHttps
