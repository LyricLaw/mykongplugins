local CookieToHeaderHandler = {
  VERSION = "1.0.0",
  PRIORITY = -1,
}

function split(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end

function trim(s)
   return s:match "^%s*(.-)%s*$"
end

function cookie_to_header(conf)
    for _, value in ipairs(conf.values) do
        local spl = split(value, "|")
        if #spl ~= 2 then
            kong.log.warn("CookieToHeader invalid config value: " .. value)
            goto continue
        end
        local cookie_name = trim(spl[1])
        local header_name = trim(spl[2])
        if cookie_name and header_name then
            cookie_string = kong.request.get_header("cookie")
            if cookie_string then
                local cookie_value = cookie_string:match(cookie_name .. "=([^;]+)")
            end
            if cookie_value then
                kong.service.request.set_header(header_name, cookie_value)
            end
        end
        ::continue::
    end
end

function CookieToHeaderHandler:rewrite(conf)
    cookie_to_header(conf)
    kong.ctx.plugin.global = true
end

function CookieToHeaderHandler:access(conf)
    if not kong.ctx.plugin.global then
        cookie_to_header(conf)
    end
end

return CookieToHeaderHandler
