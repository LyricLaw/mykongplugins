local typedefs = require "kong.db.schema.typedefs"

return {
  name = "cookie-to-header",
  fields = {
    { consumer = typedefs.no_consumer },
    { protocols = typedefs.protocols_http },
    {
      config = {
        type = "record",
        fields = {
          {
            values = {
              description = "Format: CookieName | HeaderName",
              type = "array",
              default = {},
              elements = { type = "string" }
            }
          }
        },
      },
    },
  },
}
