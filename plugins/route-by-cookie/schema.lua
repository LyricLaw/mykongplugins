local typedefs = require "kong.db.schema.typedefs"

return {
  name = "route-by-cookie",
  fields = {
    { consumer = typedefs.no_consumer },
    {
      config = {
        type = "record",
        fields = {
          { values = {
              description = "Format: CookieName | CookieValue | UpstreamName",
              type = "array",
              default = {},
              elements = { type = "string" }
            }
          }
        },
      },
    },
  },
}
