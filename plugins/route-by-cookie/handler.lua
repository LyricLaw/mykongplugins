local RouteByCookieHandler = {
  VERSION = "1.0.0",
  PRIORITY = -1,
}

function split(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end

function trim(s)
   return s:match "^%s*(.-)%s*$"
end

function RouteByCookieHandler:access(conf)
    cj = require("cjson")
    kong.log.warn("RouteByCookie conf.values: " .. cj.encode(conf.values))
    for _, value in ipairs(conf.values) do
        kong.log.warn("RouteByCookie value: " .. cj.encode(conf.values))
        local spl = split(value, "|")
        if #spl ~= 3 then
            kong.log.warn("RouteByCookie invalid config value: " .. value)
            goto continue
        end
        local cookie_name = trim(spl[1])
        kong.log.warn("RouteByCookie cookie_name: " .. cookie_name)
        local target_cookie_value = trim(spl[2])
        kong.log.warn("RouteByCookie target_cookie_value: " .. target_cookie_value)
        local real_cookie_value = kong.request.get_header("cookie"):match(cookie_name .. "=([^;]+)")
        if cookie_name and real_cookie_value == target_cookie_value then
            local upstream = trim(spl[3])
            kong.log.warn("RouteByCookie upstream: " .. upstream)
            kong.service.set_upstream(upstream)
            break
        end
        ::continue::
    end
end

return RouteByCookieHandler
