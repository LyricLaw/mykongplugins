local typedefs = require "kong.db.schema.typedefs"

return {
  name = "redirect",
  fields = {
    { consumer = typedefs.no_consumer },
    { protocols = typedefs.protocols_http },
    {
      config = {
        type = "record",
        fields = {
          { headers = { type = "array", default = {}, elements = { type = "string" } }, },
          { status_code = { type = "number", required = false, default = 302 }, },
          { redirect_url = { type = "string", required = true }, },
        },
      },
    },
  }
}
