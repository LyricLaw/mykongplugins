local RedirectHandler = {
  VERSION = "1.0.0",
  PRIORITY = 999,
}

function split(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end

function RedirectHandler:access(conf)
    local status_code = conf.status_code
    local redirect_url = conf.redirect_url
    local headers = conf.headers
    local header_table = {}
    for _, header in ipairs(headers) do
        local h = split(header, ":")
        if #h ~= 2 then
            kong.log.warn("Redirect invalid header: " .. header)
            goto continue
        end
        local header_key = h[1]
        local header_value = h[2]
        if header_key and header_value then
            header_table[header_key] = header_value
        end
        ::continue::
    end
    header_table["Location"] = redirect_url
    return kong.response.exit(status_code, "Redirecting to ".. redirect_url .. "...", header_table)
end

return RedirectHandler
