local typedefs = require "kong.db.schema.typedefs"

return {
  name = "redis_fail2ban",
  fields = {
    { consumer = typedefs.no_consumer },
    { protocols = typedefs.protocols_http },
    {
      config = {
        type = "record",
        fields = {
          {
            host = {
              description = "Redis host",
              type = "string",
              default = "redis"
            }
          },
          {
            port = {
              description = "Redis port",
              type = "number",
              default = 6379
            }
          },
          {
            second = {
              description = "Request limit in 1 second",
              type = "number",
              default = 0
            }
          },
          {
            minute = {
              description = "Request limit in 1 minute",
              type = "number",
              default = 0
            }
          },
          {
            hour = {
              description = "Request limit in 1 hour",
              type = "number",
              default = 0
            }
          },
          {
            day = {
              description = "Request Limit in 1 day",
              type = "number",
              default = 0
            }
          }
        },
      },
    },
  },
}
