local redis = require "resty.redis"
local cjson = require "cjson"

local RedisFail2banHandler = {
  VERSION = "1.0.0",
  PRIORITY = 999,
}

local time_dict = {
    second = 1,
    minute = 60,
    hour = 1440,
    day = 86400
}

local function connect(host, port)
    local redis_client = redis:new()
    redis_client:set_timeout(DEFAULT_TIMEOUT)
    local result = redis_client:connect(host, port)
    return redis_client, result
end


local function get_redis_key(suffix)
    local ip = kong.client.get_forwarded_ip()
    local key = "f2b:" .. kong.plugin.get_id() .. ":" .. ip .. ":" .. suffix
    return key
end

local function block_if_exceeded(client, conf)
    for key, val in pairs(time_dict) do

        local redis_key = get_redis_key(key)
        local value = client:get(redis_key)
        
        if conf[key] ~= 0 and value ~= ngx.null and conf[key] < tonumber(value) then
            kong.response.exit(403, "Restricted")
        end
    end
end

function RedisFail2banHandler:access(conf)
    local client, result = connect(conf.host, conf.port)
    if result and client ~= nil then
        block_if_exceeded(client, conf)
        client:close()
    end
end

local function increase_failed_count(premature, conf, redis_key_prefix)
    if premature then
        return
    end
    local client, result = connect(conf.host, conf.port)
    if result and client ~= nil then
        for key, val in pairs(time_dict) do
            local redis_key = redis_key_prefix .. key
            local value = client:get(redis_key)
            client:incr(redis_key)
            if value == ngx.null then
                client:expire(redis_key, val)
            end
        end
        client:close()
    end
end

function RedisFail2banHandler:header_filter(conf)
    local status = kong.service.response.get_status()
    if status ~= nil then
        kong.ctx.plugin.failed = (status >= 400 and status <= 599)
    end
end

function RedisFail2banHandler:log(conf)
    if kong.ctx.plugin.failed then
        ngx.timer.at(0, increase_failed_count, conf, get_redis_key(""))
    end
end
    

return RedisFail2banHandler

